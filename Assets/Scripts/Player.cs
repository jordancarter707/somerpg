﻿/********************************************
 * This will be a mouse point to click game
 * ******************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Variables
    public float movementSpeed = 0;

    public GameObject playerMovePoint = null;
    private Transform pmr = null;  //player move point obj that is instatiated on click
    private bool triggeringPMR = false;

    private bool moving = false;

    Animation anim = null;

    //Functions

    public void Start()
    {
        pmr = Instantiate(playerMovePoint.transform, this.transform.position, Quaternion.identity);
        pmr.GetComponent<BoxCollider>().enabled = false;
        anim = GetComponent<Animation>();
    }


    private void Update()
    {
        //Player Movement
        Plane playerPlane = new Plane(Vector3.up, transform.position);  // A plan that can be clicked with a ray for player movement & interaction
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);  // Detects where mosue is pointed in the scene
        float hitDistance = 0.0f;  //original distance for hit

        //Check if mouse is located in scene
        if (playerPlane.Raycast(ray, out hitDistance))
        {  //detecting if the player plane is being hit by the ray with hit distance in mind

            Vector3 mousePosition = ray.GetPoint(hitDistance); //Orignal position for the ray

            if (Input.GetMouseButtonDown(0))
            {
                moving = true;
                triggeringPMR = false;
                pmr.transform.position = mousePosition;
                pmr.GetComponent<BoxCollider>().enabled = true;
            }
        }


        if (moving)
        {
            Move();
        }
        else
        {
            Idle();
        }

        if (triggeringPMR)
        {
            moving = false;
        }
    }

    public void Move()
    {

        transform.position = Vector3.MoveTowards(transform.position, pmr.transform.position, movementSpeed);
        this.transform.LookAt(pmr.transform);

        anim.CrossFade("walk");

    }

    public void Idle()
    {
        anim.CrossFade("idle");
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PMR")
        {
            triggeringPMR = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PMR")
        {
            triggeringPMR = false;
        }
    }

}